﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Color_Detection
{
    public partial class Form1 : Form
    {
        Bitmap objBitmap;
        Bitmap objBitmap1;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult d = openFileDialog1.ShowDialog();
            if (d == DialogResult.OK)
            {
                objBitmap = new Bitmap(openFileDialog1.FileName);
                pictureBox1.Image = objBitmap;
            }
        }

        private void btnstatic_Click(object sender, EventArgs e)
        {
            objBitmap1 = new Bitmap(objBitmap);
            Color w1;
            for (int x = 1; x < objBitmap1.Width-1; x++)
                for (int y = 1; y < objBitmap1.Height - 1; y++)
                {
                    Color w = objBitmap1.GetPixel(x, y);
                    if (((w.R > 102) && (w.R < 160)) && ((w.G > 70) && (w.G < 100)) && ((w.B > 0) && (w.B > 65)))
                        w1 = Color.FromArgb(w.R, w.G, w.B);
                    else
                        w1 = Color.FromArgb(0, 0, 0);
                    objBitmap1.SetPixel(x, y, w1);
                }
            pictureBox2.Image = objBitmap1;
        }

        private void btndistance_Click(object sender, EventArgs e)
        {
            objBitmap1 = new Bitmap(objBitmap);
            Color w1; int d;
            int r = 144, g = 89, b = 65;
            for (int x = 1; x < objBitmap1.Width - 1; x++)
                for (int y = 1; y < objBitmap1.Height - 1; y++)
                {
                    Color w = objBitmap1.GetPixel(x, y);
                    d = Math.Abs(w.R - r) + Math.Abs(w.G - g) + Math.Abs(w.B - b);
                    if (d < 85)
                        w1 = Color.FromArgb(w.R, w.G, w.B);
                    else
                        w1 = Color.FromArgb(0, 0, 0);
                    objBitmap1.SetPixel(x, y, w1);
                }
            pictureBox3.Image = objBitmap1;
        }
    }
}
